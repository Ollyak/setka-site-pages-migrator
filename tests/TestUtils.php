<?php

use \PHPUnit\Framework\TestCase;
use \Setka\PagesMigrator\Utils;

class TestUtils extends TestCase
{
	public function testCanCompareSamePages()
	{
		$page1 = [
			'ID' => 1,
			'post_author' => 1,
			'post_title' => 'Post 1',
			'post_name' => 'post_1',
			'post_content' => 'Post Content',
			'acf_fields' => [
				'field1' => 'value 1',
				'field2' => [
					0 => [
						'field3' => 'value 3'
					]
				]
			]
		];

		$page2 = [
			'ID' => 2,
			'post_author' => 2,
			'post_title' => 'Post 1',
			'post_name' => 'post_1',
			'post_content' => 'Post Content',
			'acf_fields' => [
				'field1' => 'value 1',
				'field2' => [
					0 => [
						'field3' => 'value 3'
					]
				]
			]
		];

		$this->assertTrue(
			Utils::isPagesEqual($page1, $page2)
		);
	}

	public function testCannotCompareDifferentPages()
	{
		$page1 = [
			'ID' => 1,
			'post_author' => 1,
			'post_title' => 'Post 1',
			'post_name' => 'post_1',
			'post_content' => 'Post Content',
			'acf_fields' => [
				'field1' => 'value 1',
				'field2' => [
					0 => [
						'field3' => 'value 3'
					]
				]
			]
		];

		$page2 = [
			'ID' => 2,
			'post_author' => 2,
			'post_title' => 'This is the different pages',
			'post_name' => 'post_1',
			'post_content' => 'Post Content',
			'acf_fields' => [
				'field1' => 'value 1',
				'field2' => [
					0 => [
						'field3' => 'value 3'
					]
				]
			]
		];

		$this->assertFalse(
			Utils::isPagesEqual($page1, $page2)
		);
	}

	public function testCanCleanupPage()
	{
		$page = [
			'ID' => 999,
			'post_author' => 999,
			'post_title' => 'Post 1',
			'post_name' => 'post_1',
			'post_content' => 'Post Content',
			'acf_fields' => [
				'field1' => 'value 1',
				'field2' => [
					0 => [
						'field3' => 'value 3'
					]
				]
			]
		];

		$result = Utils::cleanupPage($page);

		$this->assertArrayNotHasKey('ID', $result);
		$this->assertEquals(1, $result['post_author']);
	}

	public function testCannotCleanupInvalidPageInput()
	{
		$this->expectException(Exception::class);
		Utils::cleanupPage([]);
	}
}