<?php
/**
 * Plugin Name:     Setka Site Pages Migrator
 * Plugin URI:      https://setka.io
 * Description:     Simple pages migrations utility for setka.io website
 * Author:          Setka
 * Author URI:      https://setka.io
 * Text Domain:     setka-site-pages-migrator
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Setka_Site_Pages_Migrator
 */

if (!class_exists('Setka\PagesMigrator\PagesMigrator')) {
	require_once __DIR__ . '/vendor/autoload.php';
}
