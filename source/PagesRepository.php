<?php


namespace Setka\PagesMigrator;


use Webmozart\Assert\Assert;

class PagesRepository
{
	private $config;

	public function __construct($config)
	{
		$this->config = $config;
	}

	/**
	 * Get current pages set
	 * @return array
	 */
	public function currentPages() {

		if (get_option('show_on_front') === 'page') {
			$frontPageID = (int) get_option('page_on_front');
		}

		$pages = [];
		$wp_posts = get_pages();
		foreach ($wp_posts as $wp_post) {
			$postID = $wp_post->ID;

			$page = $wp_post->to_array();
			$page['acf_fields'] = get_fields($postID);

			if (isset($frontPageID) && $frontPageID > 0 && $postID === $frontPageID) {
				$page['is_front_page'] = true;
			}

			$pages[$wp_post->post_name] = $page;
		}
		return $pages;
	}


	public function savePagesSet($pages) {
		$filename = $this->config->backupDir . 'pages_' . time() . '.txt';
		file_put_contents($filename, serialize($pages));
	}


	/**
	 * Check if page exists by post_name
	 * @param $slug
	 */
	public function isPageExists($page)
	{
		Assert::isArray($page);
		$currentPage = $this->getPageBySlug($page['post_name']);
		if(!$currentPage) {
			return false;
		}
		return  Utils::isPagesEqual($page, $currentPage);
	}


	public function getPageBySlug($post_name)
	{
		$pages = $this->currentPages();
		return isset($pages[$post_name]) ? $pages[$post_name] : null;
	}

}