<?php


namespace Setka\PagesMigrator;


use Setka\PagesMigrator\Exceptions\PagesMigratorException;
use Webmozart\Assert\Assert;

class RestoreService
{
	private $config;
	private $pagesRepository;

	public function __construct($config, PagesRepository $pagesRepository)
	{
		$this->config = $config;
		$this->pagesRepository = $pagesRepository;
	}

	/**
	 * Restore lacking pages on site from givent pages set
	 * @param array $pages
	 * @example restorePagesFromSet(currentPages());
	 */
	public function restorePagesFromSet($pages)
	{
		Assert::isArray($pages);
		$restoredPagesCount = 0;
		foreach ($pages as $page) {
			if ($this->pagesRepository->isPageExists($page)) {
				continue;
			}
			$this->restorePage($page);
			$restoredPagesCount++;
		}
		echo 'pages restored: ' . $restoredPagesCount . PHP_EOL;
	}

	/**
	 * @param array $page with elements ['ID', 'post_title', 'post_name', ...]
	 */
	public function restorePage($page) {
		Assert::isArray($page);
		$page = Utils::cleanupPage($page);
		$postID = (int) wp_insert_post($page);
		if ($postID > 0) {

			$this->restorePageMeta($page, $postID);

			if (isset($page['is_front_page']) && $page['is_front_page']) {
				$this->setFrontPage($postID);
			}

			echo 'Page ' . $page['post_name'] . ' (id ' . $postID . ') created' . PHP_EOL;
			return true;
		}
		throw new PagesMigratorException('Error on page ' . $page['post_name'] . ' creation');
	}

	/**
	 * @param array $page
	 * @param int $postID
	 * @see https://www.advancedcustomfields.com/resources/update_field/
	 */
	private function restorePageMeta($page, $postID)
	{
		Assert::isArray($page);
		Assert::integer($postID);
		Assert::greaterThan($postID, 0);

		// TODO is it enough to restore all fields settings?
		if (isset($page['acf_fields'])) {
			foreach ($page['acf_fields'] as $field => $value) {
				$isUpdated = update_field($field, $value, $postID);
				if (!$isUpdated) {
					throw new PagesMigratorException('Post ' . $postID . ' field ' . $field . ' value ' . var_dump($value, true)  . ' not updated');
				}
			}
		}
		return true;
	}


	private function setFrontPage($postID)
	{
		update_option('page_on_front', $postID);
		update_option('show_on_front', 'page');
	}
}