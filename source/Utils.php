<?php


namespace Setka\PagesMigrator;

use Webmozart\Assert\Assert;

class Utils
{
	/**
	 * Compare if pages are equal (ignoring ID, post_author)
	 * @param array $page1
	 * @param array $page2
	 *
	 * @return bool True if pages are equal. False if not equal.
	 */
	public static function isPagesEqual($page1, $page2) {
		Assert::isArray($page1);
		Assert::notEmpty($page1);
		Assert::isArray($page2);
		Assert::notEmpty($page2);
		return  self::cleanupPage($page1) === self::cleanupPage($page2);
	}

	/**
	 * Clean up site specific fields from page: ID, author ID
	 * @param array $page
	 *
	 * @return array
	 */
	public static function cleanupPage($page) {
		Assert::isArray($page);
		Assert::notEmpty($page);
		unset($page['ID']);
		$page['post_author'] = 1;
		return $page;
	}
}