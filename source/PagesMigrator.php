<?php

namespace Setka\PagesMigrator;

use Setka\PagesMigrator\Exceptions\PagesMigratorException;
use Webmozart\Assert\Assert;

/**
 * Simple pages migrations for setka.io website.
 */
class PagesMigrator {

	private $pagesRepository;
	private $backupService;
	private $migrateService;
	private $restoreService;

	public function __construct($moduleDir = false)
	{
		$config = new Config($moduleDir);
		$this->pagesRepository = new PagesRepository($config);
		$this->backupService = new BackupService($config, $this->pagesRepository);
		$this->migrateService = new MigrateService($config, $this->pagesRepository);
		$this->restoreService = new RestoreService($config, $this->pagesRepository);
	}


	/**
	 * Backup current WP pages to file.
	 */
	public function backup()
	{
		$this->backupService->backupCurrentPages();
	}


	/**
	 * Restore pages from backup file. From latest backup files by default.
	 */
	public function restore()
	{
		try {
			$pages = $this->backupService->pagesFromBackup();
			Assert::isArray($pages);
			$this->restoreService->restorePagesFromSet($pages);
		} catch (PagesMigratorException $e) {
			var_dump($e);
		}
	}


	/**
	 * Restore pages from migration file.
	 */
	public function migrate()
	{
		try {
			$this->backupService->backupCurrentPages();
			$migrationPages = $this->migrateService->migrationPages();
			$this->migrateService->deleteObsoletePages($migrationPages);
			$this->restoreService->restorePagesFromSet($migrationPages);
		} catch (PagesMigratorException $e) {
			var_dump($e);
		}
	}

}