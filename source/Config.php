<?php


namespace Setka\PagesMigrator;


class Config {
	const DEFAULT_MODULE_DIR = 'utils/pages-migrator/';
	const BACKUPS_DIR = 'backups/';
	const MIGRATIONS_DIR = 'migrations/';

	public $moduleDir;
	public $backupDir;
	public $migrationsDir;

	public function __construct($moduleDir = false)
	{
		$this->moduleDir = $moduleDir ? trailingslashit($moduleDir) : self::DEFAULT_MODULE_DIR;
		$this->backupDir = $this->backupDir();
		$this->migrationsDir = $this->migrationsDir();
	}

	private function backupDir() {
		$theme = wp_get_theme();
		$dir = trailingslashit($theme->get_template_directory()) . $this->moduleDir . self::BACKUPS_DIR;
		if (!is_dir($dir)) {
			mkdir($dir, 0755, true);
		}
		return $dir;
	}

	private function migrationsDir() {
		$theme = wp_get_theme();
		$dir = trailingslashit($theme->get_template_directory()) . $this->moduleDir . self::MIGRATIONS_DIR;
		if (!is_dir($dir)) {
			mkdir($dir, 0755, true);
		}
		return $dir;
	}
}