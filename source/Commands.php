<?php


namespace Setka\PagesMigrator;

/**
 * Static wrappers for PagesMigrator methods to run with 'wp eval' or compser
 */
class Commands {

	public static function backup() {
		$migrator = new PagesMigrator();
		$migrator->backup();
	}

	public static function restore() {
		$migrator = new PagesMigrator();
		$migrator->restore();
	}

	public static function migrate() {
		$migrator = new PagesMigrator();
		$migrator->migrate();
	}
}