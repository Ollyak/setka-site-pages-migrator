<?php


namespace Setka\PagesMigrator;


use Setka\PagesMigrator\Exceptions\PagesMigratorException;
use Webmozart\Assert\Assert;

class BackupService
{
	private $config;
	private $pagesRepository;

	public function __construct($config, PagesRepository $pagesRepository)
	{
		$this->config = $config;
		$this->pagesRepository = $pagesRepository;
	}

	/**
	 * Backup all pages if any page changed .
	 */
	public function backupCurrentPages() {
		$pages = $this->pagesRepository->currentPages();

		if ($this->isPagesAlreadySaved($pages)) {
			return true;
		};

		$this->pagesRepository->savePagesSet($pages);

		return true;
	}

	private function isPagesAlreadySaved($pages) {
		return $pages === $this->pagesFromBackup();
	}

	/**
	 * Read pages array from backup file. Last found backup file by default.
	 * @param string $backupFilename
	 *
	 * @return array|false Array of pages arrays if found. False if not found.
	 * @throws PagesMigratorException
	 */
	public function pagesFromBackup($backupFilename = '')
	{
		if (!$backupFilename) {
			$backupFilename = $this->lastBackupFilename();
		}
		Assert::string($backupFilename);
		Assert::notEmpty($backupFilename);

		$pages = unserialize(file_get_contents($this->config->backupDir . $backupFilename), ['allowed_classes' => false]);
		if (!is_array($pages)) {
			return false;
		}

		return $pages;
	}

	private function lastBackupFilename() {
		$dir = $this->config->backupDir;
		$files = scandir($dir, SCANDIR_SORT_DESCENDING);
		$lastFile = $files[0];
		if (!$lastFile) {
			throw new PagesMigratorException('no backup file found');
		}
		return $lastFile;
	}
}