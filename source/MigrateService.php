<?php


namespace Setka\PagesMigrator;


use Setka\PagesMigrator\Exceptions\PagesMigratorException;
use Webmozart\Assert\Assert;

class MigrateService
{
	private $config;
	private $pagesRepository;

	public function __construct($config, PagesRepository $pagesRepository)
	{
		$this->config = $config;
		$this->pagesRepository = $pagesRepository;
	}


	/**
	 * Delete obsolete pages from site comparing to input pages set.
	 * @param array $migrationPages
	 *
	 * @throws PagesMigratorException
	 */
	public function deleteObsoletePages($migrationPages)
	{
		Assert::isArray($migrationPages);
		$deletedPagesCount = 0;
		$currentPages = $this->pagesRepository->currentPages();
		foreach ($currentPages as $currentPage) {
			if (!isset($migrationPages[$currentPage['post_name']])
			    || !Utils::isPagesEqual($currentPage, $migrationPages[$currentPage['post_name']])
			) {
				if (wp_delete_post($currentPage['ID'])) {
					echo 'Page ' . $currentPage['post_name'] . ' (id ' . $currentPage['ID'] . ') deleted' . PHP_EOL;
					$deletedPagesCount++;
				} else {
					throw new PagesMigratorException('Page delete error');
				}
			}
		}
		echo 'obsolete pages deleted: ' . $deletedPagesCount . PHP_EOL;
	}

	private function lastMigrationFilename() {
		$dir = $this->config->migrationsDir;
		$files = scandir($dir, SCANDIR_SORT_DESCENDING);
		$lastFile = $files[0];
		if (!$lastFile) {
			throw new PagesMigratorException('no migration file found');
		}
		return $lastFile;
	}

	/**
	 * Get migrations pages set. From last migrations file by default.
	 * @param string $migrationFilename
	 *
	 * @return array pages set
	 * @throws PagesMigratorException
	 */
	public function migrationPages($migrationFilename = '')
	{
		if (!$migrationFilename) {
			$migrationFilename = $this->lastMigrationFilename();
		}

		return unserialize(file_get_contents($this->config->migrationsDir . $migrationFilename), ['allowed_classes' => false]);
	}
}