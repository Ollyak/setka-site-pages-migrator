# Simple pages migrations utility for setka.io website

Export and import Wordpress pages to file. Utility supports export/import of:
 - Page elements (title, content, slug, etc.),
 - ACF fields 
 - Front page WP option.
 

## Usage

### Backup

`wp eval 'Setka\PagesMigrator\Commands::backup();'`

Current pages will be saved in utils/page-migrator/backups/pages_[timestamp].txt as serialized php array.


### Restore

`wp eval 'Setka\PagesMigrator\Commands::restore();'`

Restore pages from latest backup file found in utils/page-migrator/backups/. All absent or changed pages will be created.
Front page WP option will be updated if backup file contained front page. 


### Migrate

1. Make backup file on source site.
2. Place backup file in utils/page-migrator/migrations/ folder on destination file.
3. Run `wp eval 'Setka\PagesMigrator\Commands::migrate();'`. 

All absent or changed pages will be created. All obsolete pages will be deleted from WP.
Front page WP option will be updated if backup file contained front page.


## Composer support

### Installation

Add following to composer.json

```
"scripts": {
         "migrate-pages-backup": "wp eval 'Setka\\PagesMigrator\\Commands::backup();'",
         "migrate-pages-restore": "wp eval 'Setka\\PagesMigrator\\Commands::restore();'",
         "migrate-pages-migrate": "wp eval 'Setka\\PagesMigrator\\Commands::migrate();'"
     }
```

### Usage with composer scripts

`composer run-script migrate-pages-backup`

`composer run-script migrate-pages-restore`

`composer run-script migrate-pages-migrate`
